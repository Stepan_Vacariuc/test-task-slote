#!/bin/bash

# directory to backup
SOURCE_DIR=
BACKUP_DIR=
BACKUP_PATH=
USER=
HOST=
DATETIME="$(date '+%Y-%m-%d_%H:%M')"
INCREMENTAL=

usage() { echo "$0 usage:" && grep " .)\ #" $0; exit 0; }
[ $# -eq 0 ] && usage
while getopts "hd:s:u:r:i" arg; do
  case $arg in
    d) #Direcroty for backups
            BACKUP_DIR=$OPTARG
            echo "Destination backup path $BACKUP_DIR"
            ;;
    s) #Source direcroty to be backed up
            SOURCE_DIR=${OPTARG}
            echo "Source backup path ${SOURCE_DIR}"
            ;;
    u) #Remote user to be used
            USER=${OPTARG}
            echo "Remote user ${USER}"
            ;;
    r) #Remote host or IP
            HOST=${OPTARG}
            echo "Remote host ${HOST}"
            ;;
    i) #If set then incremental
            INCREMENTAL=1
            ;;
    h | *) #Display help.
      usage
      exit 0
      ;;
  esac
done
shift "$(($OPTIND -1))"

LATEST_LINK="${BACKUP_DIR}/latest"

if [[ $INCREMENTAL ]]
then
        echo "Perform incremental backup"
        BACKUP_PATH=$BACKUP_DIR/inc-${DATETIME}
        mkdir -p "${BACKUP_PATH}"
        rsync -ravz --delete $USER@$HOST:"${SOURCE_DIR}/" --link-dest "${LATEST_LINK}" "${BACKUP_PATH}"
else
        echo "Perform full backup"
        BACKUP_PATH=$BACKUP_DIR/full-$DATETIME
        mkdir -p "${BACKUP_PATH}"

        rsync -ravz $USER@$HOST:"${SOURCE_DIR}/" "${BACKUP_PATH}"
fi

rm -rf $LATEST_LINK
ln -s "${BACKUP_PATH}" "${LATEST_LINK}"