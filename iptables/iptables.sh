#!/bin/bash

yum install iptables-services -y
systemctl enable iptables
systemctl start iptables

yum update -y

cd ../etc/rsyslog.d

echo ":msg,contains,\"[admin_inbound]:\" -/var/log/admin_inbound.log" > 00-iptables.conf
echo ":msg,contains,\"[db_n_docker_inbound]:\" -/var/log/db_n_docker_inbound.log" >> 00-iptables.conf
echo ":msg,contains,\"[server_outbound_custom]:\" -/var/log/server_outbound_custom.log" >> 00-iptables.conf
echo ":msg,contains,\"[std_inbound]:\" -/var/log/standart_inbound.log" >> 00-iptables.conf
echo ":msg,contains,\"[std_outbound]:\" -/var/log/standart_outbound.log" >> 00-iptables.conf
echo "& stop" >> 00-iptables.conf

systemctl restart rsyslog.service

#Flush and clean filter and nat tables
iptables -t filter -F
iptables -t filter -X
iptables -t nat -F
iptables -t nat -X

############################## Log chains implemented not for all filter chains due to lack of time.
#Chain for admin logging
iptables -N LOG_ADMIN_INBOUND
iptables -A LOG_ADMIN_INBOUND -j LOG --log-prefix "[admin_inbound]: " --log-level 6
iptables -A LOG_ADMIN_INBOUND -j RETURN

#Chain for db/docker logging
iptables -N LOG_DB_INBOUND
iptables -A LOG_DB_INBOUND -j LOG --log-prefix "[db_n_docker_inbound]: " --log-level 6
iptables -A LOG_DB_INBOUND -j RETURN

#Chain for outbound traffic logging
iptables -N LOG_OUTBOUND_CUSTOM
iptables -A LOG_OUTBOUND_CUSTOM -j LOG --log-prefix "[server_outbound_custom]: " --log-level 6
iptables -A LOG_OUTBOUND_CUSTOM -j RETURN

#Chain for standart inbound drop
iptables -N LOG_STD_INBOUND_DROP
iptables -A LOG_STD_INBOUND_DROP -j LOG --log-prefix "[std_inbound]: " --log-level 6
iptables -A LOG_STD_INBOUND_DROP -j DROP

#Chain for standart inbound drop
iptables -N LOG_STD_OUTBOUND_DROP
iptables -A LOG_STD_OUTBOUND_DROP -j LOG --log-prefix "[std_outbound]: " --log-level 6
iptables -A LOG_STD_OUTBOUND_DROP -j DROP
###############################

###############################
#Chain to allow particular admin source addresses
iptables -N inbound-chain-admin
iptables -A inbound-chain-admin -j LOG_ADMIN_INBOUND
iptables -A inbound-chain-admin -s 172.31.48.11 -j ACCEPT
iptables -A inbound-chain-admin -s 18.128.0.0/8 -j ACCEPT
iptables -A inbound-chain-admin -j RETURN

#Chain to allow particular DB/Docker source addresses
iptables -N inbound-chain-db-n-docker
iptables -A inbound-chain-db-n-docker -j LOG_DB_INBOUND
iptables -A inbound-chain-db-n-docker -s 172.32.32.32 -j ACCEPT
iptables -A inbound-chain-db-n-docker -s 172.31.31.31 -j ACCEPT
iptables -A inbound-chain-db-n-docker -j RETURN

#Chain to allow particular user addresses per request
iptables -N incoming-chain-user-request
iptables -A incoming-chain-user-request -s 172.30.0.0 -j ACCEPT
iptables -A incoming-chain-user-request -j RETURN

#Chain to allow particular user with temporary permission addresses  #verify
iptables -N chain-user-tmp-access
iptables -A chain-user-tmp-access -p tcp --dport 22 -s 172.31.48.11 -m time --timestart 02:00 --timestop 03:00 -j ACCEPT
iptables -A chain-user-tmp-access -p icmp --icmp-type any -s 172.31.48.11 -m time --timestart 02:00 --timestop 03:00 -j ACCEPT
iptables -A chain-user-tmp-access -j RETURN

#Chain to allow particular outbound traffic
iptables -N outbound-chain-traffic
iptables -A outbound-chain-traffic -p tcp --dport 80  -j ACCEPT
iptables -A outbound-chain-traffic -p tcp --dport 443 -j ACCEPT
iptables -A outbound-chain-traffic -p tcp --dport 22  -j ACCEPT
iptables -A outbound-chain-traffic -p tcp --dport 53  -j ACCEPT
iptables -A outbound-chain-traffic -p udp --dport 53  -j ACCEPT
iptables -A outbound-chain-traffic -p tcp --dport 123 -j ACCEPT
iptables -A outbound-chain-traffic -p udp --dport 123 -j ACCEPT
iptables -A outbound-chain-traffic -p icmp            -j ACCEPT
iptables -A outbound-chain-traffic -j LOG_OUTBOUND_CUSTOM

#Chain to keep-alive created connections
iptables -N chain-connections
iptables -A chain-connections -p all -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT
iptables -A chain-connections -j RETURN
###############################


###############################
#Loopback accepted
iptables -A INPUT  -i lo -j ACCEPT
iptables -A OUTPUT -o lo -j ACCEPT

#Accept in/out packets for exisitng connections
iptables -A INPUT  -j chain-connections
iptables -A OUTPUT -j chain-connections

#Accept admin connection
iptables -A INPUT -j inbound-chain-admin

#Accept db/docker connections
iptables -A INPUT -j inbound-chain-db-n-docker

#Accept specific users connections
iptables -A INPUT -j incoming-chain-user-request

#Accept temporary users connections
iptables -A INPUT -j chain-user-tmp-access

#Accept server outbound connections
iptables -A OUTPUT -j outbound-chain-traffic

#Drop and log else
iptables -A INPUT -j LOG_STD_INBOUND_DROP
iptables -A OUTPUT -j LOG_STD_OUTBOUND_DROP

#Drop everything else
iptables -P INPUT   DROP
iptables -P FORWARD DROP
iptables -P OUTPUT  DROP
###############################